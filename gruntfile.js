'use strict';

var path = require('path');

module.exports = function(grunt) {
  // Unified Watch Object
  var lessPaths = [
        __dirname + '/less',
        __dirname + '/bower_components',
        path.resolve(__dirname + '/..')
      ]
    , files = {
        main: ['less/main.less'],
        app: ['less/app.less'],
      };

  // Project Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      less: {
        files: files.main.concat(files.app),
        tasks: ['less', 'cssmin'],
        options: {
        }
      }
    },
    cssmin: {
      all: {
        files: {
          'build/main.min.css': 'build/main.css',
          'build/app.min.css': 'build/app.css'
        }
      }
    },
    less: {
      compile: {
        options: {
          outputSourceFiles: true,
          paths: lessPaths,
          relativeUrls: true,
          strictMath: false,
          sourceMap: true,
          //sourceMapFilename: 'public/css/<%= pkg.name %>.css.map'
        },
        files: {
          'build/main.css': 'less/main.less',
          'build/app.css': 'less/app.less',
        }
      }
    }
  });

  // Load NPM tasks
  require('load-grunt-tasks')(grunt);

  // Making grunt default to force in order not to break the project.
  grunt.option('force', true);

  // Default task(s).
  grunt.registerTask('default', ['build', 'watch']);

  // Build task(s).
  grunt.registerTask('build', ['less', 'cssmin']);
};
